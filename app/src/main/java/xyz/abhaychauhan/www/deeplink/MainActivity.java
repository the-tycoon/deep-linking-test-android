package xyz.abhaychauhan.www.deeplink;

import android.bluetooth.BluetoothAdapter;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import xyz.abhaychauhan.www.deeplink.activity.AnalyticActivity;
import xyz.abhaychauhan.www.deeplink.analytics.AnalyticsApplication;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Context context = MainActivity.this;

    Button buttonLinkFirst;
    Button buttonLinkSecond;
    Button buttonLinkThird;
    Button buttonAnalytics;

    TextView deviceView;

    ClipboardManager clipboardManager;
    ClipData clipData;

    ImageView imageViewMain;


    private static final String FIRST_LINK = "https://www.udacity.com/course/android-basics-data-storage--ud845";
    private static final String SECOND_LINK = "https://www.udacity.com/course/android-basics-networking--ud843";
    private static final String THIRD_LINK = "https://www.udacity.com/course/software-architecture-design--ud821";
    private static final String POSTER_LINK = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ43hTYIjTSuGtpB-g4KQdV3rBfL4zC5x6cm_fW4LAa_VoRLG__";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean RunsInEmulator = false;

        buttonLinkFirst = (Button) findViewById(R.id.button_link_first);
        buttonLinkSecond = (Button) findViewById(R.id.button_link_second);
        buttonLinkThird = (Button) findViewById(R.id.button_link_third);
        buttonAnalytics = (Button) findViewById(R.id.analyticsButton);

        deviceView = (TextView) findViewById(R.id.device_view);

        imageViewMain = (ImageView) findViewById(R.id.imageViewMain);
        Picasso.with(this).load(POSTER_LINK).into(imageViewMain);

//        buttonLinkFirst.setOnClickListener(this);
//        buttonLinkSecond.setOnClickListener(this);
//        buttonLinkThird.setOnClickListener(this);
        buttonAnalytics.setOnClickListener(this);

//        clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
//        try{
//            //deviceView.setText("Mac Address : " + URLEncoder.encode(CommonUtils.getMacAddr(),"UTF-8"));
//
//        }catch (UnsupportedEncodingException e){
//
//        }
//        deviceView.setText(Settings.Secure.getString(context.getContentResolver(),
//                Settings.Secure.ANDROID_ID));
        CommonUtils cu = new CommonUtils(this);
        deviceView.setText(CommonUtils.getDeviceId());
    }

    @Override
    public void onClick(View view) {
//        if (view == findViewById(R.id.button_link_first)) {
//            clipData = ClipData.newPlainText("First Button Clip", FIRST_LINK);
//        }
//        if (view == findViewById(R.id.button_link_second)) {
//            clipData = ClipData.newPlainText("Second Button Clip", SECOND_LINK);
//        }
//        if (view == findViewById(R.id.button_link_third)) {
//            clipData = ClipData.newPlainText("Third Button Clip", THIRD_LINK);
//        }
//        clipboardManager.setPrimaryClip(clipData);
//        Toast.makeText(this, "Link Copied", Toast.LENGTH_SHORT).show();
        if(view == findViewById(R.id.analyticsButton)){
            Intent intent = new Intent(MainActivity.this, AnalyticActivity.class);
            startActivity(intent);
        }
    }

}
