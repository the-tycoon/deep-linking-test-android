package xyz.abhaychauhan.www.deeplink;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
//Change Last 2 Void with String
public class vNativeMobileConversions extends AsyncTask<String, Void, Void> {
    //Done
    private final String LOG_TAG = vNativeMobileConversions.class.getName();
    //Done
    private ArrayList<String> keysList = new ArrayList<String>();
    private boolean isDebugMode = false;
    private Context myContext = null;
    String offerID = null;
    //Done
    public final String PREFS_NAME = "VNATIVE_CONVERSION_PREFERENCES_TAG";
    public final String SENT_EVENTS_KEY = "SENT_EVENTS_KEY_STRING";
    public final String REQU_IDS_KEY = "SESSION_REQUEST_ID";

    public vNativeMobileConversions(Context context) {
        myContext = context;
    }

    //Add a conversion key to the conversion that will be sent. This will be inserted into the conversion URL when it is sent.
    //You can add as many conversion keys as is desired.
    public void addConversionKey(String conversionKey) {
        keysList.add(conversionKey.replaceAll("\\s", ""));
    }

    //For this function to work properly ensure that this is declared in the AndroidManifest.xml file
    //<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    private boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) this.myContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null /*&& ni.isAvailable()*/ && ni.isConnected()){
            return true;
        }else{
            return false;
        }
    }

    private JSONArray getSentEventURLS(){
        SharedPreferences settings = myContext.getSharedPreferences(PREFS_NAME, 0);
        String jsonString = settings.getString(SENT_EVENTS_KEY, null);
        if(jsonString == null){
            return null;
        }
        JSONArray listOfEventURLS = null;
        try {
            listOfEventURLS = new JSONArray(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listOfEventURLS;
    }

    public void addReqIDToList(String reqIDString, String offerString) {
        JSONArray existingReqIDs = this.getStoredREQIDs();
        if(existingReqIDs == null) {
            existingReqIDs = new JSONArray();
        }

        JSONObject newReqID = new JSONObject();
        for(int ii = 0; ii < existingReqIDs.length(); ii++) {
            JSONObject existingReq = null;
            try {
                existingReq = (JSONObject) existingReqIDs.get(ii);
                //don't double add offer IDs
                if(existingReq != null &&
                        existingReq.get("OFFER").equals(offerString) &&
                        existingReq.get("REQID").equals(reqIDString)) {
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            newReqID.put("OFFER", offerString);
            newReqID.put("REQID", reqIDString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        existingReqIDs.put(newReqID);

        SharedPreferences settings = myContext.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(REQU_IDS_KEY, existingReqIDs.toString());
        editor.commit();
    }

    private String getReqIDForOfferString(String offerString) {
        JSONArray existingReqIDs = this.getStoredREQIDs();
        if(existingReqIDs == null) {
            return null;
        }

        for(int ii = 0; ii < existingReqIDs.length(); ii++) {
            JSONObject existingReq = null;
            try {
                existingReq = (JSONObject) existingReqIDs.get(ii);
                if(existingReq != null &&
                        existingReq.get("OFFER").equals(offerString)) {
                    if(this.isDebugMode) {
                        System.out.println("We have a stored Request ID: " + (String) existingReq.get("REQID"));
                    }
                    return (String) existingReq.get("REQID");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private JSONArray getStoredREQIDs() {
        SharedPreferences settings = myContext.getSharedPreferences(PREFS_NAME, 0);
        String jsonString = settings.getString(REQU_IDS_KEY, null);
        if(jsonString == null) {
            return null;
        }
        JSONArray listOfEventURLS = null;
        try {
            listOfEventURLS = new JSONArray(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listOfEventURLS;
    }


    public void sendEvent(String baseURL, boolean allowDuplicates) {
        if(this.isOnline() && (allowDuplicates ||
                (!allowDuplicates && !this.hasURLBeenSentAndAddIfHasnt(baseURL)))){
            this.execute(baseURL);
        }
        else if(this.isDebugMode) {
            System.out.println("Not sending event because of duplicate or lack of internet.");
        }
    }

    //Combine the base URL with other required parameters such as the postback format
    //as well as any conversion keys which have been added
    //
    //NOTE: this function assumes that the base URL contains the format of http[s]://sometrackingdomain.com/p.ashx?o=12345
    //       with any other additional parameters added
    private String buildFinalUrl(String baseURL) {
        StringBuilder finalString = new StringBuilder(baseURL);
        for(String key : keysList) {
            finalString.append("&k=" + key);
        }

        Uri uri = Uri.parse(baseURL);
        if(uri != null) {
            offerID = uri.getQueryParameter("o");
            if(offerID != null) {
                String reqID = this.getReqIDForOfferString(offerID);
                if(reqID != null) {
                    if(uri.getQueryParameter("r") == null) {
                        finalString.append("&r=" + reqID);
                    }
                }
            }
        }

        if(this.isDebugMode) {
            System.out.println("URL:  " + finalString.toString());
        }
        return finalString.toString();
    }

    public void parseResponseString(String response) {
        if(this.isDebugMode) {
            System.out.println("response" + response);
        }

        String code = null;
        String requestID = null;

        try {
            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(new StringReader(response));
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if(eventType == XmlPullParser.START_TAG) {
                    if(parser.getName().toString().equalsIgnoreCase("request_session_id")) {
                        requestID = parser.nextText();
                    }else if(parser.getName().toString().equalsIgnoreCase("code")) {
                        code = parser.nextText();
                    }
                }
                eventType = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(code != null && code.equalsIgnoreCase("0") && requestID != null && offerID != null) {
            this.addReqIDToList(requestID, offerID);
        }
    }

    private boolean hasURLBeenSentAndAddIfHasnt(String eventURL){
        JSONArray listOfEventURLS = this.getSentEventURLS();
        if(listOfEventURLS == null) {
            listOfEventURLS = new JSONArray();
        }
        boolean isFound = false;
        for(int ii = 0; ii < listOfEventURLS.length(); ii ++){
            try {
                if(listOfEventURLS.getString(ii).equals(eventURL)){
                    isFound = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(!isFound){
            listOfEventURLS.put(eventURL);
            SharedPreferences settings = myContext.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(SENT_EVENTS_KEY, listOfEventURLS.toString());
            editor.commit();
            return false;
        }else{
            return true;
        }
    }

    //Setting this to true will make the conversion always send
    public void setDebugMode(boolean isDebug){
        isDebugMode = isDebug;
    }

    public void sendInstallConversion(String urlString) {
        if(this.isOnline() && !this.hasURLBeenSentAndAddIfHasnt(urlString)) {
            if(urlString == null) {
                System.out.println("No conversion sent, base url is null");
                return;
            }
            this.execute(urlString);
        }
        else if(this.isDebugMode) {
            System.out.println("Not sending install conversion because of lack of internet or duplicate");
        }
    }

    /**
     * createUrl create and return URL object of String url
     *
     * @param stringUrl
     * @return
     */

    private URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.d(LOG_TAG, "Not able to create URL!!");
        }
        return url;
    }

    //Change Void return type with String
    @Override
    protected Void doInBackground(String... strings) {
        //Toast.makeText(myContext, "Second 1 Test", Toast.LENGTH_SHORT).show();
        URL url = createUrl(strings[0]);
        if (url == null) {
            return null;
        }

        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(15000);
            urlConnection.connect();
            if (urlConnection.getResponseCode() == 200) {
                //Toast.makeText(myContext, "Request fired!!", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {

        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
}
