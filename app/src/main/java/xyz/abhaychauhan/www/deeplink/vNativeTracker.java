package xyz.abhaychauhan.www.deeplink;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.widget.Toast;

import java.net.URLEncoder;

public class vNativeTracker extends BroadcastReceiver {

    CommonUtils commonUtils;
    public static final String BASE_URL = "http://track.vnative.com/app?";
    public static final String DEVICE_ID = "device[name]";
    public static final String DEVICE_MAC = "device[mac]";
    public static final String DEVICE_IP = "device[ip]";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (intent.hasExtra("referrer")) {
                vNativeMobileConversions vc = new vNativeMobileConversions(context);
                commonUtils = new CommonUtils(context);

                String device_param = URLEncoder.encode(DEVICE_ID, "UTF-8") + "=" + CommonUtils.getDeviceId();
                device_param += "&" + URLEncoder.encode(DEVICE_MAC, "UTF-8") + "=" + CommonUtils.getMacAddr();
                device_param += "&" + URLEncoder.encode(DEVICE_IP, "UTF-8") + "=" + CommonUtils.getLocalIpAddress();

                String final_url = CommonUtils.getReferrerStringWithoutClickId(intent.getStringExtra("referrer")) +
                        CommonUtils.getClickIdString(intent.getStringExtra("referrer")) + "&" + device_param;

                if (!CommonUtils.deviceIsEmulator()) {
                    vc.addConversionKey(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
                    vc.sendInstallConversion(BASE_URL + final_url);
                    Toast.makeText(context, BASE_URL + final_url, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
        }
    }
}
