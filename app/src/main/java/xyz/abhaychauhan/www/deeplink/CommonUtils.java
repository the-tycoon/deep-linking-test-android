package xyz.abhaychauhan.www.deeplink;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.text.format.Formatter;
import android.util.Log;

import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class CommonUtils {

    public static Context mContext;

    public CommonUtils(Context context){
        mContext = context;
    }

    /**
     * This function check whether the Application is running on Real Device or emulator.
     * This function requires android.permission.BLUETOOTH permission to function.
     *
     * @return
     */
    public static boolean deviceIsEmulator() {
        int rating = 0;

        if (Build.PRODUCT.contains("sdk") ||
                Build.PRODUCT.contains("Andy") ||
                Build.PRODUCT.contains("ttVM_Hdragon") ||
                Build.PRODUCT.contains("google_sdk") ||
                Build.PRODUCT.contains("Droid4X") ||
                Build.PRODUCT.contains("sdk_x86") ||
                Build.PRODUCT.contains("sdk_google") ||
                Build.PRODUCT.contains("vbox86p")) {
            rating++;
        }

        if (Build.MANUFACTURER.equals("unknown") ||
                Build.MANUFACTURER.equals("Genymotion") ||
                Build.MANUFACTURER.contains("Andy") ||
                Build.MANUFACTURER.contains("TiantianVM")) {
            rating++;
        }

        if (Build.BRAND.equals("generic") ||
                Build.BRAND.equals("generic_x86") ||
                Build.BRAND.equals("TTVM") ||
                Build.BRAND.contains("Andy")) {
            rating++;
        }

        if (Build.DEVICE.contains("generic") ||
                Build.DEVICE.contains("generic_x86") ||
                Build.DEVICE.contains("Andy") ||
                Build.DEVICE.contains("ttVM_Hdragon") ||
                Build.DEVICE.contains("Droid4X") ||
                Build.DEVICE.contains("generic_x86_64") ||
                Build.DEVICE.contains("vbox86p")) {
            rating++;
        }

        if (Build.MODEL.equals("sdk") ||
                Build.MODEL.equals("google_sdk") ||
                Build.MODEL.contains("Droid4X") ||
                Build.MODEL.contains("TiantianVM") ||
                Build.MODEL.contains("Andy") ||
                Build.MODEL.equals("Android SDK built for x86_64") ||
                Build.MODEL.equals("Android SDK built for x86")) {
            rating++;
        }

        if (Build.HARDWARE.equals("goldfish") ||
                Build.HARDWARE.equals("vbox86") ||
                Build.HARDWARE.contains("ttVM_x86")) {
            rating++;
        }

        if (Build.FINGERPRINT.contains("generic/sdk/generic") ||
                Build.FINGERPRINT.contains("generic_x86/sdk_x86/generic_x86") ||
                Build.FINGERPRINT.contains("Andy") ||
                Build.FINGERPRINT.contains("ttVM_Hdragon") ||
                Build.FINGERPRINT.contains("generic_x86_64") ||
                Build.FINGERPRINT.contains("generic/google_sdk/generic") ||
                Build.FINGERPRINT.contains("vbox86p") ||
                Build.FINGERPRINT.contains("generic/vbox86p/vbox86p")) {
            rating++;
        }

        try {
            String opengl = android.opengl.GLES20.glGetString(android.opengl.GLES20.GL_RENDERER);
            if (opengl != null && opengl.contains("Bluestacks")) {
                rating += 10;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File sharedFolder = new File(Environment
                    .getExternalStorageDirectory().toString()
                    + File.separatorChar
                    + "windows"
                    + File.separatorChar
                    + "BstSharedFolder");

            if (sharedFolder.exists()) {
                rating += 10;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            BluetoothAdapter m_BluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            String m_bluetoothAdd = m_BluetoothAdapter.getAddress();
            if (m_bluetoothAdd == null) {
                rating += 3;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rating > 3;
    }

    /**
     * This function takes referrer string as argument and return the vnclick_id part only
     * irrespective of its position in the string.
     *
     * @param referrerString
     * @return
     */
    public static String getClickIdString(String referrerString) {
        String clickIdString = "&vnclick_id=";
        int vnClickIdValueStringIndex = referrerString.indexOf("vnclick_id") + 11;
        String clickIdValueString = referrerString.substring(vnClickIdValueStringIndex);
        boolean isOnlyClickId = true;
        int indexOfNextParam = 0;
        try {
            if (clickIdValueString.contains("%")) {
                isOnlyClickId = false;
                indexOfNextParam = clickIdValueString.indexOf("%");
            } else if (clickIdValueString.contains("&")) {
                isOnlyClickId = false;
                indexOfNextParam = clickIdValueString.indexOf("&");
            }
        } catch (Exception e) {

        }
        if (!isOnlyClickId) {
            clickIdString += clickIdValueString.substring(0, indexOfNextParam);
        } else {
            clickIdString += clickIdValueString.substring(0);
        }
        return clickIdString;
    }

    /**
     * This function takes referrer string as parameter and returns referrer string without
     * including vnclick_id parameter
     *
     * @param referrerString
     * @return
     */
    public static String getReferrerStringWithoutClickId(String referrerString) {
        String referrerStringWithoutClickId = "";
        int indexOfClickId = referrerString.indexOf("vnclick_id");
        if (referrerString.charAt(indexOfClickId - 1) == '&') {
            referrerStringWithoutClickId += referrerString.substring(0, indexOfClickId - 1);
        } else if (referrerString.charAt(indexOfClickId - 3) == '%') {
            referrerStringWithoutClickId += referrerString.substring(0, indexOfClickId - 3);
        }
        return referrerStringWithoutClickId;
    }

    /**
     * This function returns the mac address of the device (Compatible with Android 6.0 and above)
     *
     * @return
     */
    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

    /**
     * Function return the IP Address of the device
     *
     * @return
     */
    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("Main", "***** IP=" + ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("MAIN", ex.toString());
        }
        return null;
    }

    public static String getDeviceId(){
        String deviceId = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return deviceId;
    }

}
