package xyz.abhaychauhan.www.deeplink;

import android.os.Parcel;
import android.os.Parcelable;

public class Course implements Parcelable {

    private String courseId;
    private String courseImageUrl;
    private String courseName;
    private String courseSummary;

    public Course(String courseId, String courseImageUrl,
                  String courseName, String courseSummary) {
        this.courseId = courseId;
        this.courseImageUrl = courseImageUrl;
        this.courseName = courseName;
        this.courseSummary = courseSummary;
    }

    public Course(Parcel in) {
        this.courseId = in.readString();
        this.courseImageUrl = in.readString();
        this.courseName = in.readString();
        this.courseSummary = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.courseId);
        parcel.writeString(this.courseImageUrl);
        parcel.writeString(this.courseName);
        parcel.writeString(this.courseSummary);
    }

    public static final Parcelable.Creator<Course> CREATOR = new Parcelable.Creator<Course>() {
        public Course createFromParcel(Parcel in) {
            return new Course(in);
        }

        public Course[] newArray(int size) {
            return new Course[size];
        }
    };

    public String getCourseId() {
        return this.courseId;
    }

    public String getCourseImageUrl() {
        return this.courseImageUrl;
    }

    public String getCourseName() {
        return this.courseName;
    }

    public String getCourseSummary() {
        return this.courseSummary;
    }

}
