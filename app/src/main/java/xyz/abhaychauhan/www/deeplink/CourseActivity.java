package xyz.abhaychauhan.www.deeplink;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class CourseActivity extends AppCompatActivity {

    ImageView imageView;
    TextView courseIdView;
    TextView courseNameView;
    TextView courseSummaryView;

    Course ud845 = new Course("ud845",
            "https://lh6.ggpht.com/57QPWoDnN4dSulg4tqC_pFHcN5pmUIzyrXyu3TiirP1JRCQ36fzz6IgEPl2HdMZ3GIrc80rRzAkRiPUyOXM=s0#w=1724&h=1060",
            "Android Basics : Data Storage by Google",
            "In this course, you will learn about the importance of data persistence when building an Android app. We'll introduce you to the fundamentals of SQL, the programming language needed to interact with an SQLite relational database. SQLite is a commonly used method to store large sets of data locally on an Android device.\n" +
                    "\n");

    Course ud843 = new Course("ud843",
            "https://lh3.googleusercontent.com/Hp4UrpBlkIwM1VvtEOVAOaTZf9C6X0FXX0lKu3jllapJIlIw5hK_3tDzXamszoTC0PG_cswnx8y6EUl1aQ=s0#w=360&h=220",
            "Android Basics: Networking by Google",
            "This course is a part of the Android Basics Nanodegree by Google.\n" +
                    "\n" +
                    "Android apps are everywhere and learning to build them can be a fantastic career move. Continue on your Android app development education and learn to build multi-screen apps!\n" +
                    "\n" +
                    "This course is designed for students who have completed the Android for Beginners course and the Android Basics: Multiscreen Apps course. You don’t need any programming experience besides that course!");

    Course ud821 = new Course("ud821",
            "https://lh3.googleusercontent.com/VIkSp8zDuuOf4m6ceNkAFbvC_oiUYPI7ZG98KMpu_0GNmIXNUyfdEEzuQmqCKkeOm47TVAnM9C9NxmCT6B8=s0#w=1440&h=807",
            "Software Architecture & Design",
            "Software Architecture and Design teaches the principles and concepts involved in the analysis and design of large software systems. ");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        imageView = (ImageView) findViewById(R.id.image_view);
        courseNameView = (TextView) findViewById(R.id.course_name_textview);
        courseSummaryView = (TextView) findViewById(R.id.course_summary_textview);
        courseIdView = (TextView) findViewById(R.id.course_id_textview);
        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        String action = intent.getAction();
        String data = intent.getDataString();
        if (intent.ACTION_VIEW.equals(action) && data != null) {
            int index = data.indexOf("--ud");
            String courseId = data.substring(index + 2);
            switch (courseId) {
                case "ud845":
                    updateUI(ud845);
                    break;

                case "ud843":
                    updateUI(ud843);
                    break;

                case "ud821":
                    updateUI(ud821);
                    break;

                default:
                    updateUI(ud845);
            }
        }
    }

    private void updateUI(Course course) {
        Picasso.with(this).load(course.getCourseImageUrl().toString()).into(imageView);
        courseIdView.setText("Course Id : " + course.getCourseId());
        courseNameView.setText(course.getCourseName());
        courseSummaryView.setText(course.getCourseSummary());
    }
}
