package xyz.abhaychauhan.www.deeplink.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import xyz.abhaychauhan.www.deeplink.R;
import xyz.abhaychauhan.www.deeplink.analytics.AnalyticsApplication;

public class AnalyticActivity extends AppCompatActivity {

    public static final String TAG = AnalyticActivity.class.getSimpleName();
    private Tracker mTracker;
    public String name = "Analytic Screen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytic);

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "Setting screen name: " + name);
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
